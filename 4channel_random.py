"""
20151120 - Created by Michael Dill (c)

This sketch is designed test a 4-channel relay by cycling through each module in
a series of patterns, then increasing or decreasing the speed after each cycle.

Once a keyboard interrupt is noticed each GPIO will turn off at the end.

"""

import time
import RPi.GPIO as GPIO

# Clear all used variables
x = None
i = None

# Set the GPIO pins for each individual relay
            #  R1  R2  R3  R4
relay_list1 = [11, 13, 15, 16]
relay_list2 = [11, 15, 13, 16]

# Set time delay between relay changes
t = [.25, .125, .0625]

GPIO.setmode(GPIO.BOARD)

# Set each relay pin as an output
for x in relay_list1:
    GPIO.setup(x, GPIO.OUT)

try:
    while True:
        for i in t:
            for x in relay_list1:
                GPIO.output(x, GPIO.HIGH)
                time.sleep(i)
            for x in relay_list1:
                GPIO.output(x, GPIO.LOW)
                time.sleep(i)
            for x in relay_list2:
                GPIO.output(x, GPIO.HIGH)
                time.sleep(i)
            for x in relay_list2:
                GPIO.output(x, GPIO.LOW)
                time.sleep(i)

            # Reverse/re-reverse bothe lists before repeating
            relay_list1.reverse()
            relay_list2.reverse()

except KeyboardInterrupt:
    print("\nA keyboard interrupt has been detected!")

except:
    print("\nAn error or exception has occurred!")

finally:
    GPIO.cleanup()
